<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function index()
    {
        $this->load->view('login');
    }

    public function check_login()
    {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        if (($email == "admin@admin.com") && ($password == "admin")) {
            redirect(base_url('dashboard'));
        } else {
            redirect(base_url());
        }
    }
}
